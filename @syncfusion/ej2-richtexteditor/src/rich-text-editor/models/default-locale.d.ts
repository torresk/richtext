/**
 * Export default locale
 */
export declare let defaultLocale: {
    [key: string]: string;
};
export declare let toolsLocale: {
    [key: string]: string;
};
export declare let fontNameLocale: {
    [ket: string]: string;
}[];
export declare let formatsLocale: {
    [ket: string]: string;
}[];
