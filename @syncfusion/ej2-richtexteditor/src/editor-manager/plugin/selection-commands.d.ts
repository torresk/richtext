export declare class SelectionCommands {
    /**
     * applyFormat method
     *
     * @param {Document} docElement - specifies the document
     * @param {string} format - specifies the string value
     * @param {Node} endNode - specifies the end node
     * @param {string} value - specifies the string value
     * @param {string} selector - specifies the string
     * @returns {void}
     * @hidden

     */
    static applyFormat(docElement: Document, format: string, endNode: Node, value?: string, selector?: string): void;
    private static insertCursorNode;
    private static getCursorFormat;
    private static removeFormat;
    private static insertFormat;
    private static applyStyles;
    private static getInsertNode;
    private static getChildNode;
    private static applySelection;
    private static GetFormatNode;
    private static updateStyles;
}
