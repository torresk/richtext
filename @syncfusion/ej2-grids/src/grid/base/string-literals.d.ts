/**
 * Specifies class names
 */
/** @hidden */
export declare const rowCell: string;
/** @hidden */
export declare const gridHeader: string;
/** @hidden */
export declare const gridContent: string;
/** @hidden */
export declare const gridFooter: string;
/** @hidden */
export declare const headerContent: string;
/** @hidden */
export declare const movableContent: string;
/** @hidden */
export declare const movableHeader: string;
/** @hidden */
export declare const frozenContent: string;
/** @hidden */
export declare const frozenHeader: string;
/** @hidden */
export declare const content: string;
/** @hidden */
export declare const table: string;
/** @hidden */
export declare const row: string;
/** @hidden */
export declare const gridChkBox: string;
/** @hidden */
export declare const editedRow: string;
/** @hidden */
export declare const addedRow: string;
/**
 * Specifies repeated strings
 */
/** @hidden */
export declare const changedRecords: string;
/** @hidden */
export declare const addedRecords: string;
/** @hidden */
export declare const deletedRecords: string;
/** @hidden */
export declare const leftRight: string;
/** @hidden */
export declare const frozenRight: string;
/** @hidden */
export declare const frozenLeft: string;
/** @hidden */
export declare const ariaColIndex: string;
/** @hidden */
export declare const ariaRowIndex: string;
/** @hidden */
export declare const tbody: string;
/** @hidden */
export declare const colGroup: string;
