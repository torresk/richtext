"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pkgName = '@syncfusion/ej2-angular-richtexteditor';
exports.pkgVer = '^19.1.59';
exports.moduleName = 'RichTextEditorModule';
exports.themeVer = '~19.1.59';
