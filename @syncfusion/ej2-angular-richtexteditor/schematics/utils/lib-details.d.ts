export declare const pkgName = "@syncfusion/ej2-angular-richtexteditor";
export declare const pkgVer = "^19.1.59";
export declare const moduleName = "RichTextEditorModule";
export declare const themeVer = "~19.1.59";
