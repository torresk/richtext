export const pkgName = '@syncfusion/ej2-angular-richtexteditor';
export const pkgVer = '^19.1.59';
export const moduleName = 'RichTextEditorModule';
export const themeVer = '~19.1.59';
